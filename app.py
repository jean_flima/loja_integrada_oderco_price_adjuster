
import glob
import pandas as pd
import xml.etree.ElementTree as elt

arquivo_loja = glob.glob("*.xlsx")
arquivo_oderco = glob.glob("*.xml")

print(".............Iniciando integração.....................................\n\n")

print(".............Lendo arquivo da loja....................................\n\n")
produtos_loja_integrada = arquivo_loja[0]

print(".............Lendo arquivo Oderço.....................................\n\n")

produtos_oderco = arquivo_oderco[0]

content = elt.parse(produtos_oderco).getroot()

df = pd.read_excel(produtos_loja_integrada)

print(".............Iniciando tratamento dos dados...........................\n\n")
def get_info(inputID, coluna, flag):
    for child in content:
        if child.find('codigo').text == inputID:
            data = child.find(coluna).text            
            
            if flag == 'preco-cheio':
                return float(data) * 1.3
            elif flag == 'preco-custo':
                return float(data)
            elif flag == 'estoque':
                if data == 'S':
                    return int(3)
                else:
                    return int(0)


df['preco-custo'] = df['sku'].apply(lambda x: get_info(x,'preco', 'preco-custo' ))
df['preco-cheio'] = df['sku'].apply(lambda x: get_info(x,'preco', 'preco-cheio'))
df['estoque-quantidade'] = df['sku'].apply(lambda x: get_info(x,'estoque', 'estoque'))

print(".............Tratamento finalizado....................................\n\n")

print(".............Iniciando gravação do arquivo............................\n\n")
df.to_excel('arquivo_atualizado.xlsx', index=False)

print(".............Arquivo 'arquivo_atualizado.xlsx' salvo na sua pasta.....\n\n")