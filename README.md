# loja_integrada_oderco_price_adjuster

Projeto para atualizar os preços de todos os produtos cadastrados na loja integrada com os dados da Oderco

O app lê o arquivo XLSX com o template e produtos cadastrados da loja integrada, e cruza com os dados do XLS da Oderco

Verifica a posição de estoque disponível e calcula o preço com o lucro, calcula o valor final e grava na planilha para ser importado novamente na loja integrada.

